# FROM rails:onbuild

FROM ruby:2.5.0

# throw errors if Gemfile has been modified since Gemfile.lock
# RUN bundle config --global frozen 1

# ------- add
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev node.js vim less

RUN groupadd --gid 1000 app \
    && useradd --uid 1000 --gid app --shell /bin/bash --create-home app

ENV LANG=ja_JP.UTF-8
ENV LANGUAGE=ja_JP.UTF-8
ENV TZ=Asia/Tokyo

RUN mkdir /app
WORKDIR /app
ADD Gemfile .
ADD Gemfile.lock .

ENV BUNDLE_JOBS=4
RUN bundle install

USER app